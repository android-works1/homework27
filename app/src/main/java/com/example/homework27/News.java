package com.example.homework27;

import android.graphics.drawable.Drawable;

public class News {
    String title;
    String text;
    String siteName;
    String date;
    String imageUrl;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }



    public News(String title, String text, String siteName, String date, String imageUrl) {
        this.title = title;
        this.text = text;
        this.siteName = siteName;
        this.date = date;
        this.imageUrl = imageUrl;

    }
}
