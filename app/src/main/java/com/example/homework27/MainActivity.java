package com.example.homework27;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycleView);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        List<News> newsList = new ArrayList<>();
        newsList.add(new News("Wi-Fi Alliance введет новый стандарт Wi-Fi 6E с частотой 6 ГГц", "Базирующийся в Остине(штат Техас, США) Wi-Fi Alliance официально объявил о планируемом вводе нового стандарта частоты 6 ГГц для использования в беспроводных технологиях. В настоящее время стандарт Wi-Fi 6 (IEEE 802.11ax) поддерживает работу устройств в двух частотных диапазонах Wi-Fi — 2,4 и 5 ГГц.", "24gadget.ru", "1 день назад","https://24gadget.ru/uploads/posts/2020-01/thumbs/1578423602_wi-fi-6e.jpg"));
        newsList.add(new News("Евросоюз собирается заставить Apple отказаться от разъема Lightning", "Европейская комиссия вновь настаивает на использовании единого метода зарядки для мобильных устройств.", "nv.ua", "2 дня назад", "https://images.weserv.nl/?q=85&output=webp&bg=white&url=https://nv.ua/system/Article/posters/002/133/142/900x450/b608916d47340d84fbac3abcb648b49b.jpeg?stamp=20200114133530"));
        newsList.add(new News("Tele2 и Huawei заключили контракт о прямых поставках", "Прямые поставки смартфонов под брендом Huawei в Tele2 начнутся с конца января 2020 года. Прежде они осуществлялись официальным дистрибьютором вендора в РФ.", "it-world.ru", "1 день назад", "https://www.it-world.ru/upload/iblock/d77/resize_936_0_697575.jpg"));

    DataAdapter adapter = new DataAdapter(newsList, this);
    recyclerView.setAdapter(adapter);

    }
}