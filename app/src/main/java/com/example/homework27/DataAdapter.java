package com.example.homework27;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<ListViewHolder> implements View.OnClickListener{

    private List<News> newsList;
    private LayoutInflater inflater;
    private Context context;

    public DataAdapter(List<News> newsList, Context context) {
        this.context=context;
        this.newsList = newsList;
        this.inflater=LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.item_layout, parent, false);
        ListViewHolder viewHolder=new ListViewHolder(view);
        viewHolder.itemView.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        News news= newsList.get(position);
        holder.title.setText(news.getTitle());
        holder.text.setText(news.getText());
        holder.siteName.setText(news.getSiteName());
        holder.date.setText(news.getDate());
        Picasso.get().load(news.getImageUrl()).into(holder.imageView);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(context,"ItemClick", Toast.LENGTH_LONG).show();
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }
}
class ListViewHolder extends RecyclerView.ViewHolder{
    ImageView imageView;
    TextView title;
    TextView text;
    TextView siteName;
    TextView date;

    public ListViewHolder(@NonNull View itemView){
        super(itemView);

         imageView=itemView.findViewById(R.id.imageView);
         title=itemView.findViewById(R.id.titleView);
         text=itemView.findViewById(R.id.textView);
        siteName=itemView.findViewById(R.id.siteNameView);
        date=itemView.findViewById(R.id.dateView);
    }
}